import lightkurve as lk
import matplotlib.pyplot as plt
import numpy as np
import transitleastsquares as tls
import math

#Relevant Parameters
path = '*' #Saving path
target = "TIC 183979262" #Name of the object (Do not use TOI names)###
instrument = "TESS" #'Kepler' or 'TESS' or 'K2'###
csvname = target + ".csv" #Name of the saved file
allQS = False #False for only one quarter/sector/campaign, True for all quarters/sectors/campaigns
qs = [i for i in range(28,29)] #If allQS = False, specify which quarter/sector/campaign. Otherwise irrelevant


#Periodogram Parameters
periodo = False #Create a periodogram?
period_min = 4.0 #Lower period bound
period_max = 7.0 #Upper period bound
oversampling_factor = 5 #default 3
duration_grid_step = 1.02 #default 1.1
transit_template = 'default' #'default', 'grazing' or 'box'
T0_fit_margin = 0.01 #default = 0.01
mask_max_period = 0 #Remove strongest signal and create a new periodogram mask_max_period times. default = 0

#Editing-Parameters
rmv_nans = True #Remove NANs?
kappa = 0 #kappa value for the kappa*sigma clipping. kappa = 0 for no kappa*sigma clipping. default = 5
normalize = True #Normalize flux values?

#Clip Parameters
clip = False #Clip a certain time interval?
start = 1356 #Lower bound
stop = 13800 #Upper bound

#Lightkurve-internal Parameters
window_length = 101 #Window length for the flattening, 0 for no flattening
PDC = True #False for SAP_Flux, True for PDCSAP_Flux.
cadence = 'long' #'long' or 'short'. Default = 'long'


def lghtkrv(target, instrument, path = '*', csvname = target + ".csv", allQS = True, qs = 0, window_length = 0, PDC = True, clip = False, start = 0, stop = 0, periodo = False, period_min = 0, period_max = 0, oversampling_factor = 3, duration_grid_step = 1.1, transit_template = 'default', T0_fit_margin = 0.01, mask_max_period = 0, rmv_nans = True, kappa = 0, normalize = True, cadence = 'long'):
    #Download all or just one quarter/sector/campaign?
    if(allQS == True):
        #lightcurve-files download
        lc = lk.search_lightcurvefile(target, mission=instrument, cadence=cadence).download_all()
        if(lc is None):
            print("Error! Target not found! If you used TOI names, use TIC instead.")
            return

        #Choose flux type
        if(PDC == True):
            lc = lc.PDCSAP_FLUX
        else:
            lc = lc.SAP_FLUX

        #Stitching and flattening
        if(window_length != 0):
            flat = lc.stitch(corrector_func=lambda lc:lc.flatten(window_length=window_length))
        else:
            flat = lc.stitch()  

    else:
        #Data download
        if(instrument == "Kepler"):
            lc = lk.search_lightcurvefile(target, mission=instrument, quarter=qs, cadence=cadence).download_all()
        elif(instrument == "TESS"):
            lc = lk.search_lightcurvefile(target, mission=instrument, sector=qs, cadence=cadence).download_all()
        elif(instrument == "K2"):
            lc = lk.search_lightcurvefile(target, mission=instrument, campaign=qs, cadence=cadence).download_all()
        
        if(lc is None):
            print("Error! Target not found! If you used TOI names, use TIC instead.")
            return
        
        #Choose flux type
        if(PDC == True):
            lc = lc.PDCSAP_FLUX
        else:
            lc = lc.SAP_FLUX

        #Stitching and flattening
        if(window_length != 0):
            flat = lc.stitch(corrector_func=lambda lc:lc.flatten(window_length=window_length))
        else:
            flat = lc.stitch()  
            
    #NANs and outlier removal and normalization
    if (rmv_nans == True):
        flat = flat.remove_nans()
    if (kappa != 0):
        flat = flat.remove_outliers(sigma = kappa)
    if (normalize == True):
        flat = flat.normalize()
    
    #clipping
    if(clip == True):
        flat = flat[(flat.time < start) | (flat.time > stop)]
    
    flat.errorbar(label=target)
    
    #Add offset and write to csv
    if(instrument == "Kepler" or instrument == "K2"):
        flat.time += 2454833.0
    if(instrument == "TESS"):
        flat.time += 2457000.0
    if(path == '*'):
        with open(csvname,'w') as file:
            for i in range(len(flat.time)):
                if not math.isnan(flat.flux[i]):
                    file.write('%s,%s,%s\n'%(flat.time[i],flat.flux[i],flat.flux_err[i]))
    else:
        with open(path + csvname,'w') as file:
            for i in range(len(flat.time)):
                if not math.isnan(flat.flux[i]):
                    file.write('%s,%s,%s\n'%(flat.time[i],flat.flux[i],flat.flux_err[i]))
    
    plt.show()
    
    #Create a periodogram
    if(periodo == True):       
        model = tls.transitleastsquares(flat.time, flat.flux)
        results = model.power(period_min = period_min, period_max = period_max, oversampling_factor=oversampling_factor, duration_grid_step=duration_grid_step, transit_template=transit_template, T0_fit_margin = T0_fit_margin)
        
        #Print results
        print('Period', format(results.period, '.5f'), 'd')
        print(len(results.transit_times), 'Transit times in time series:', \
              ['{0:0.5f}'.format(i) for i in results.transit_times])
        print('Transit depth', format(results.depth, '.5f'))
        print('Best duration (days)', format(results.duration, '.5f'))
        print('Signal detection efficiency (SDE):', results.SDE)
        
        #Plot periodogram
        plt.figure()
        ax = plt.gca()
        ax.axvline(results.period, alpha=0.4, lw=3)
        plt.xlim(np.min(results.periods), np.max(results.periods))
        for n in range(2, 10):
            ax.axvline(n*results.period, alpha=0.4, lw=1, linestyle="dashed")
            ax.axvline(results.period / n, alpha=0.4, lw=1, linestyle="dashed")
        plt.ylabel(r'SDE')
        plt.xlabel('Period (days)')
        plt.plot(results.periods, results.power, color='black', lw=0.5)
        plt.xlim(0, max(results.periods))

        plt.show()

        #Remove strongest signal and create a new periodogram
        while(mask_max_period != 0): 
            intransit = tls.transit_mask(flat.time, results.period, 2*results.duration, results.T0)
            flat.flux = flat.flux[~intransit]
            flat.time = flat.time[~intransit]
            flat.time, flat.flux = tls.cleaned_array(flat.time, flat.flux)
            model = tls.transitleastsquares(flat.time, flat.flux)
            results = model.power(period_min = period_min, period_max = period_max, oversampling_factor=oversampling_factor, duration_grid_step=duration_grid_step, transit_template=transit_template, T0_fit_margin = T0_fit_margin)
       
            #Print results
            print('Period', format(results.period, '.5f'), 'd')
            print(len(results.transit_times), 'Transit times in time series:', \
                  ['{0:0.5f}'.format(i) for i in results.transit_times])
            print('Transit depth', format(results.depth, '.5f'))
            print('Best duration (days)', format(results.duration, '.5f'))
            print('Signal detection efficiency (SDE):', results.SDE)
        
            #Plot periodogram
            plt.figure()
            ax = plt.gca()
            ax.axvline(results.period, alpha=0.4, lw=3)
            plt.xlim(np.min(results.periods), np.max(results.periods))
            for n in range(2, 10):
                ax.axvline(n*results.period, alpha=0.4, lw=1, linestyle="dashed")
                ax.axvline(results.period / n, alpha=0.4, lw=1, linestyle="dashed")
                plt.ylabel(r'SDE')
                plt.xlabel('Period (days)')
                plt.plot(results.periods, results.power, color='black', lw=0.5)
                plt.xlim(0, max(results.periods))

            plt.show()
            mask_max_period -= 1

if __name__ == '__main__':  
    lghtkrv(target, instrument, path = path, csvname = csvname, allQS = allQS, qs = qs, window_length = window_length, PDC = PDC, clip = clip, start = start, stop = stop, periodo = periodo, period_min = period_min, period_max = period_max, oversampling_factor = oversampling_factor, duration_grid_step = duration_grid_step, transit_template = transit_template, T0_fit_margin = T0_fit_margin, mask_max_period = mask_max_period, rmv_nans = rmv_nans, kappa = kappa, normalize = normalize, cadence = cadence)


